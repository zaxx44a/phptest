<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', 'EnrolmentController@index')->name('index'); //Main Page Shows Students
Route::get('create', 'EnrolmentController@create')->name('add'); //Add Enrolment form page
Route::post('store', 'EnrolmentController@store')->name('store'); //route's that get the Enrolment form data and save to DB then redirect to index
