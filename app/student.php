<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    //
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * Get all of the posts for the country.
     */
    public function courses()
    {
        return $this->hasMany('App\enrolment', 'student_id', 'id')
            ->join('courses', 'courses.id', '=', 'enrolments.course_id')
            ->select('courses.name', 'enrolments.student_id', 'enrolments.course_id')
        ;
    }
}
