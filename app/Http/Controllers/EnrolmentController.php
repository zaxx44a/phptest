<?php

namespace App\Http\Controllers;

use App\course;
use App\enrolment;
use App\Http\Controllers\Controller;
use App\student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EnrolmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Html view with students
     */
    public function index()
    {
        /**
         * Get all the students and return it to the view to make
         * the show it to visitor
         */
        $students = student::with('courses')->get();
        return view('students.index', ['students' => $students]);
    }

    /**
     * Show the form for creating a new enrolment.
     *
     * @return Html view with courses
     */
    public function create()
    {
        /**
         * Get all the courses and return it to the view to make
         * the auto complete ,then inject data to enrolment create view
         */
        $courses = course::all();
        return view('enrolment.create', ['courses' => $courses]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return redirect to index
     */
    public function store(Request $request)
    {
        //input validator rules
        $validator = Validator::make($request->all(), [
            'photo'   => 'required|image:jpeg,png|dimensions:min_width=400,min_height=300',
            'name'    => 'required',
            'courses' => 'required',
            'email'   => 'required',
        ])->validate();
        //find student by name ,if not found create new
        $student          = student::firstOrNew(['name' => $request->name]);
        $student->country = $request->country;
        //upload photo input
        $student->avatar = str_replace("public/", "", $request->file('photo')->store('public'));
        $student->email  = $request->email;
        $student->phone  = $request->phone;
        $student->save();
        //save every course to that user one by one in many to many relationship
        $courses = explode(',', $request->courses);
        foreach ($courses as $key => $value) {
            $enrolment = enrolment::firstOrCreate([
                'student_id' => $student->id,
                'course_id'  => course::where('name', $value)->first()->id,
            ]);

        }
        return redirect()->route('index');
    }

}
