@extends('layouts.main')
@section('content')
  <div class="container-fluid text-center">
    <div class="row content">
      <div class="col-sm-12 text-left">
        <h1>Add Enrolment</h1>
        <!-- Authentication Links -->
        <form action="{{route('store')}}" method="post" class="form-horizontal" enctype="multipart/form-data" >
          <!-- for CSRF Check -->
          {{ csrf_field() }}
          <!-- name Input Field -->
          <div class="form-group">
            <label class="control-label col-sm-2" for="name">Name:</label>
            <div class="col-sm-6">
              <input name="name" value="{{ old('name') }}" data-validation="length" data-validation-length="min1" type="text" class="form-control" id="name" placeholder="Enter Name" novaliadate />
                <!-- name Backend Error Display-->
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <!-- email Input Field -->
          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email:</label>
            <div class="col-sm-6">
              <input name="email" value="{{ old('email') }}" type="email" class="form-control" id="email" placeholder="Enter email"  data-validation="email" novaliadate />
                <!-- email Backend Error Display-->
                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <!-- phone Input Field -->
          <div class="form-group">
            <label class="control-label col-sm-2" for="phone">Phone:</label>
            <div class="col-sm-6">
              <input name="phone" value="{{ old('phone') }}" type="text" class="form-control" id="phone" placeholder="Enter Phone " data-validation="number" data-validation-allowing="+-. " novaliadate />
                <!-- phone Backend Error Display-->
                @if ($errors->has('phone'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <!-- country Input Field -->
          <div class="form-group">
            <label class="control-label col-sm-2" for="country">Country:</label>
            <div class="col-sm-6">
              <input name="country" value="{{ old('country') }}" type="text" class="form-control" data-validation="length" data-validation-length="min1"    id="country" placeholder="Enter Country" novaliadate />
                <!-- country Backend Error Display-->
                @if ($errors->has('country'))
                    <span class="help-block">
                        <strong>{{ $errors->first('country') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <!-- courses Input Field -->
          <div class="form-group">
            <label class="control-label col-sm-2" for="courses">Courses:</label>
            <div class="col-sm-6">
              <input name="courses" value="{{ old('courses') }}" type="text" data-validation="length" data-validation-length="min3" class="form-control example_typeahead" id="courses" placeholder="Enter Courses" novaliadate />(add [arabic,english,database,physics])
                <!-- courses Backend Error Display-->
                @if ($errors->has('courses'))
                    <span class="help-block">
                        <strong>{{ $errors->first('courses') }}</strong>
                    </span>
                @endif
            </div>
          </div>
          <!-- photo Input Field -->
          <div class="form-group">
            <label class="control-label col-sm-2" for="photo">User Photo:</label>
            <div class="col-sm-6">
              <input name="photo" type="file" class="form-control" id="photoInput" placeholder="Enter User Photo" data-validation="dimension mime" data-validation-allowing="png,jpeg" data-validation-min-dimension="400X300" value="{{ old('photo') }}" />
              <span id="photo-error"></span>
                <!-- photo Backend Error Display-->
                @if ($errors->has('photo'))
                    <span class="help-block">
                        <strong>{{ $errors->first('photo') }}(minimum 300X400)</strong>
                    </span>
                @endif
                <div id="imgContainer"></div>
            </div>
          </div>
          <div class="form-group">
            <div class="col-sm-offset-2 col-sm-6">
              <button type="submit" class="btn btn-default">Submit</button>
            </div>
          </div>
      </form>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      // Fetch Courses From PHP array to JS Array via blade
      var courses = [
           @foreach($courses as $c)
              {name:"{{$c->name}}",id:{{$c->id}}},
           @endforeach
      ];
  </script>
@endsection('content')
@section('scripts')

    <script src="{{ mix('/js/app.js') }}"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
  //validator for all input except the image dimension
  $.validate({
    modules : 'file'
  });

  //validator for  image dimension
  window.URL = window.URL || window.webkitURL;

    $("form").submit( function( e ) {
      $('#photo-error').text("")
        var form = this;
        e.preventDefault(); //Stop the submit for now
        //Replace with your selector to find the file input in your form
        var fileInput = $(this).find("input[type=file]")[0],
            file = fileInput.files && fileInput.files[0];

        if( file ) {
            var img = new Image();

            img.src = window.URL.createObjectURL( file );

            img.onload = function() {
                var width = img.naturalWidth,
                    height = img.naturalHeight;

                window.URL.revokeObjectURL( img.src );

                if( width >= 400 && height >= 300 ) {
                    form.submit();
                }
                else {
                    $('#photo-error').text("Your image's dimension must be greater than 400X300 ")
                }
            };
        }
        else {
        //No file was input or browser doesn't support client side reading
            form.submit();
        }

    });
</script>
@endsection('scripts')
