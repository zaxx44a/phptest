@extends('layouts.main')
@section('content')


<div class="container-fluid bg-3 text-center row">
  <h3>Students</h3><br>
  <!-- students Repreater-->
  @foreach($students as $student)
   <div class="col-sm-4 col-md-3">
    <div class="thumbnail">
     <img src="{{asset('storage/'.$student->avatar)}}" class="img-responsive img-thumbnail" style="width:100%" alt="Image">
      <div class="caption text-left">
        <h3 class="text-center">{{$student->name}}</h3>
        <p>phone :{{$student->phone}}</p>
        <p>email :{{$student->email}}</p>
        <p>country :{{$student->country}}</p>
        <p> courses :
          @foreach($student->courses as $course)
           {{$course->name}},
          @endforeach
        </p>
      </div>
    </div>
  </div>
    @endforeach
  <!-- show when there is no students-->

    @if(count($students)==0)
    <div class="jumbotron">
      <h1>Sorry, No students Untill Now!</h1>

      <p><a class="btn btn-primary btn-lg" href="{{ route('add') }}" role="button">Add Some</a></p>
    </div>
    @endif
  </div>
@endsection('content')
