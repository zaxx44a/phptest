
/**
 * Typeahead
 */
var courses_array = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  local :courses
});
courses_array.initialize();

var elt = $('.example_typeahead');
elt.tagsinput({
  typeaheadjs: {
    name: 'courses_array',
    displayKey: 'name',
    valueKey: 'name',
    source: courses_array.ttAdapter(),
  },
  freeInput:false
});



