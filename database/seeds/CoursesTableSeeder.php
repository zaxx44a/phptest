<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \DB::table('courses')->insert([
            ['name' => 'English', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Arabic', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Physics', 'created_at' => date("Y-m-d H:i:s")],
            ['name' => 'Databases', 'created_at' => date("Y-m-d H:i:s")],
        ]);
    }
}
