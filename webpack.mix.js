let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.babel('resources/assets/js/library/*.*', 'public/js/vendor.js')
	.styles([
	    'resources/assets/css/bootstrap-tagsinput.css',
	    'resources/assets/css/bootstrap.min.css',
	    'resources/assets/css/font-awesome.min.css',
	    'resources/assets/css/bootstrap-theme.min.css',
	    'resources/assets/css/tags.css'
	], 'public/css/all.css')
	.js('resources/assets/js/app.js', 'public/js')
   // .sass('resources/assets/sass/app.scss', 'public/css');
